$(document).ready(function(){
    

    (function($){
        $(window).on('load', function(){
            $.instagramFeed({
                'username': 'creativasites',
                'container': "#instagram-feed-demo",
                'display_profile': false,
                'display_biography': false,
                'display_gallery': true,
                'get_raw_json': false,
                'callback': null,
                'styling': true,
                'items': 36,
                'items_per_row': 36,
                'margin': 0
            });
        });
    })(jQuery);


    $('.modalidades-slide').owlCarousel({
        loop: false,
        margin: 0,
        autoplay: false,
        nav: false,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
      });

      
    $('.projetos-slide').owlCarousel({
        loop: false,
        margin: 0,
        autoplay: false,
        nav: false,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
      });

      $('.quem-somos-slide').owlCarousel({
        loop: false,
        margin: 0,
        dots: false,
        nav: false,
        autoplay: true,
        smartSpeed: 700,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
      });

      $('.depoimentos-slide').owlCarousel({
        loop: true,
        margin: 30,
        dots: true,
        nav: false,
        autoplay: true,
        smartSpeed: 700,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            1200: {
                items: 2
            }
        }
      });

      $('.banner-slide').owlCarousel({
        loop: true,
        margin: 0,
        dots: true,
        nav: false,
        autoplay: true,
        smartSpeed: 700,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
      });

      $('.logos-slide').owlCarousel({
        loop: true,
        margin: 0,
        dots: true,
        nav: false,
        autoplay: true,
        smartSpeed: 700,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
      });

      $('.logos-slide-mobile').owlCarousel({
        loop: true,
        margin: 0,
        dots: false,
        nav: true,
        autoplay: true,
        smartSpeed: 700,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 2
            },
            1200: {
                items: 2
            }
        }
      });

      var checkExist = setInterval(function() {
        if ($('.instagram_gallery').length) {
           console.log("Exists!");
           $('.instagram_gallery').addClass('owl-carousel');
           $('.instagram_gallery').addClass('owl-theme');
     
         
            $('.instagram_gallery').owlCarousel({
                loop: true,
                rewind: true,
                margin: 0,
                nav: false,
                navText: [
                    '<i class="fas fa-chevron-left"></i>',
                    '<i class="fas fa-chevron-right"></i>'
                ],
                dots: false,
                autoplay: true,
                smartSpeed: 700,
                autoplayTimeout: 5000,
                autoplayHoverPause: true,
                responsive: {
                    0: {
                        items: 3
                    },
                    1200: {
                        items: 6
                    }
                }
            });
     
   
           clearInterval(checkExist);
        }
     }, 100);

  
})  

new MenuChef('.menu-chef a', {
    theme: {
      theme: 'side', 
      pageEffect: 'blur',
      effectOnOpen: 'smooth'  
    }
  })


// $(window).scroll(function(){
//     let scroll = $(window).scrollTop();
//       if(scroll> 700){
//         $('.menu').css('visibility','visible');
//         $('.menu').css('opacity','1');
        
//       }else{
//         $('.menu').css('visibility','hidden');
//         $('.menu').css('opacity','0');
//       }
//   })
